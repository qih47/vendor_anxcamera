# ANXCamera
## About branches :
- v190 with custom watermark feature for sirius
- v190-xtended only for MSM-Xtended rom with no support custom watermark feature
## Getting Started :
### Setup
- use this command before start build "export BUILD_BROKEN_DUP_RULES=true" or
- copy this "BUILD_BROKEN_DUP_RULES := true" to device tree Boardconfig.mk
### Cloning :
- Clone this repo in vendor/ANXCamera in your working directory by :
```
git clone https://qih47@bitbucket.org/qih47/vendor_anxcamera.git -b <branch> vendor/ANXCamera
```
